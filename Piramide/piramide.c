#include <stdio.h>
#include <stdlib.h>

int main()
{
    char piramide[40][79];
    int linha = 1, coluna = 1, contador1 = 0, contador2 = 0, A1 = 0,
               A2 = 0, B1 = 0, B2 = 0, andares = 0, andares1 = 0;
    char  exit = 0;

    printf("Este programa cria uma piramide de até 40 andares!!\n");

    while(exit != 27)
    {
        do
        {
            printf("Digite o numero de andares da sua piramide até no maximo 40:");
            scanf("%d",&andares);

            if(andares > 40)
            {
                printf("Numero de andares maior que 40, por favor diminua o numero de andares \n");
            }

        }while(andares > 40);

        andares1 = 1 + ((andares - 1) * 2);

        for(linha=0;linha<40;linha++)
        {
            for(coluna=0;coluna<79;coluna++)
            {
                piramide[linha][coluna]=' ';
            }
        }
        A2 = andares1/2;
        B2 = andares1/2;
        //******************* CRIA AS LATERAIS DA PIRÂMIDE ********************
        for(linha=0; linha < andares; linha++)
        {
            A2++;
            B2--;
            for(contador2 = andares1/2; contador2 < A2 && A2 != andares1+1; contador2++)
            {
                piramide[linha][contador2] = '.';
                for(contador1 = andares1/2; contador1 > B2 && B2 > -2; contador1--)
                {
                    piramide[linha][contador1] = '.';
                }
            }
        }
        //******************* CRIA O MEIO DA PIRÂMIDE *************************
        for(linha = 0; linha < andares; linha++)
        {
            for(coluna = 0; coluna < andares1; coluna++)
            {
                piramide[linha][(andares1/2)] = '.';

                printf("%c",piramide[linha][coluna]);
            }
            printf("\n");
        }
        //	printf("Parabens, voce so demorou uma eternidade pra fazer essa porra :v \n");
        printf("\nAperta qualquer tecla para criar outra piramide ou aperte 'ESC' para sair\n");
        exit = getchar();
    }
    return 0;
}
