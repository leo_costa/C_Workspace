#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * @brief Para uso quando não existe sprintf com %f
 *
 * @param str[]
 * @param ndigit
 * @param val
 */
void float_to_str(char str[], int ndigit, float val)
{
//     short int first_digits = 1, second_digits = 0;
//     int integer = 0, decimals = 0;
//     int i_val = (int)val;
//     integer = i_val;
//     decimals = (int)((val - integer)*1e9);
//
//     printf("val = %f | i = %d | d = %d\n", val, integer, decimals);
//
//     while((i_val /= 10) != 0)
//         first_digits++;
//     if(ndigit >= first_digits)
//         second_digits = ndigit - first_digits;
//     else
//         printf("Number too large for conversion\n");
//
//     char i[15], d[15];
// //     itoa(integer, i, 10);
//     sprintf(i, "%d", integer);
//
//     char negative_decimal = decimals < 0;
//     if(negative_decimal)
//     {
//         decimals *= -1;
//         if(integer > 0)
//         {
//             char i_aux[16];
//             sprintf(i_aux, "-%s", i);
//             sprintf(i, "%s", i_aux);
//         }
//         first_digits++;
//     }
// //     itoa(decimals, d, 10);
//     sprintf(d, "%d", decimals);
//
//     i[first_digits] = 0;
//     d[second_digits] = 0;
//     if(second_digits)
//         sprintf(str, "%s.%s", i, d);
//     else
//         sprintf(str, "%s", i);
}

/**
 * @brief Para uso quando há sprintf com %f
 *
 * @param str[]
 * @param ndigit
 * @param val
 */
void float_to_str_2(char str[], int ndigit, float val)
{
    int first_digits = 1, second_digits = 0;
    int i_val = (int)val;

    while((i_val /= 10) != 0)
        first_digits++;

    if(ndigit >= first_digits)
        second_digits = ndigit - first_digits;
    else
        printf("Number too large\n");

    char format[8] = "%", aux[6];
    sprintf(aux, "%d.%df", first_digits, second_digits);
    strcat(format, aux);

    sprintf(str, format, val);
}

void reverse_str(char in[], char out[])
{
    int sz = strlen(in)-1, i;
    for(i = 0; i <= sz; ++i)
        out[i] = in[sz-i];
    out[i] = 0;
}

int insert_dot(char val[], int dot_pos)
{
    char aux_val[20];
    sprintf(aux_val, "%s", val);
    int sz = strlen(val), i, offset = 0, j=0, ret = 0;

    for(i = 0; i <= sz+1; ++i)
    {
        if(dot_pos <= 0)
        {
            aux_val[i] = '0';
            aux_val[i+1] = '.';
            ret = 1;
            offset = 2;

            while(dot_pos + j < 0)
            {
                aux_val[i + 2 + j] = '0';
                offset++;
                j++;
            }

            dot_pos = 999;
        }
        else if(i == dot_pos)
        {
            aux_val[i] = '.';
            ret = 1;
            offset = 1;
        }

        aux_val[i + offset] = val[i];
    }
    sprintf(val, "%s", aux_val);
    return ret;
}

void int64_to_str(char str[], int ndigit, long int val, int ten_factor)
{
    int digits = 0, offset = 0;
    long int aux_val = val;

    if(aux_val < 0)
    {
        aux_val *= -1;
        offset = 1;
        str[0] = '-';
    }

    char aux_str[20];

    while(aux_val != 0)
    {
        aux_str[digits] = aux_val % 10 + '0';
        aux_val /= 10;
        digits++;
    }
    aux_str[digits] = 0;
    reverse_str(aux_str, &str[offset]);
    int dot = insert_dot(&str[offset], digits - ten_factor);

    if(digits - ten_factor > ndigit)
        dot = 0;

    if(strlen(str) - offset - dot >= ndigit)
        str[ndigit+offset+dot] = 0;

    digits = strlen(str)-1;
    if(str[digits] == '.')
        str[digits] = 0;
}

int main()
{
    long int data = -12884901888;
//     long int data = -1288;
    char string_data[20];
//     float data = 233.007;
    printf("data: %ld\n", data);

//     float_to_str(string_data, 9, data);
    int64_to_str(string_data, 9, data, 6);
    char d_aux[] = "0";
    int test = insert_dot(d_aux, -5);
    printf("rest: %s\n", string_data);

    return 0;
}
