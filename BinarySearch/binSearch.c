#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef unsigned int uint32_t;
typedef short int uint8_t;

#define _TEMPERATURE_N_SAMPLES 156

const uint32_t _TEMPERATURE_ADC_READ[] = {110, 111, 112, 113, 114, 115, 116,
    117, 118, 119, 120, 121, 122, 123, 124, 125, 127, 128, 129, 130, 131, 133,
    134, 135, 137, 138, 140, 141, 143, 144, 146, 147, 149, 151, 152, 154, 156,
    158, 159, 161, 163, 165, 167, 170, 172, 174, 176, 179, 181, 183, 186, 189,
    191, 194, 197, 200, 203, 206, 209, 212, 216, 219, 223, 227, 230, 234, 239,
    243, 247, 252, 257, 262, 267, 272, 278, 284, 290, 296, 303, 310, 317, 325,
    333, 341, 350, 359, 369, 379, 390, 402, 414, 427, 440, 455, 471, 488, 506,
    515, 525, 535, 546, 557, 569, 581, 594, 607, 621, 635, 650, 665, 680, 696,
    712, 730, 748, 767, 788, 803, 819, 836, 853, 871, 890, 910, 931, 953, 975,
    999, 1024, 1042, 1061, 1081, 1101, 1122, 1144, 1167, 1191, 1215, 1241,
    1260, 1280, 1300, 1321, 1343, 1365, 1388, 1412, 1437, 1463, 1489, 1517,
    1540, 1563, 1588, 1613, 1638};

uint8_t _binary_search(uint8_t min, uint8_t max, uint32_t target)
{
    uint8_t avg = (uint8_t)floor((min+max)/2.0);
    if(min > max)
        return avg;
    printf("i=%d, v=%d\n", avg,_TEMPERATURE_ADC_READ[avg]);

    if(_TEMPERATURE_ADC_READ[avg] == target)
        return avg;
    else if(_TEMPERATURE_ADC_READ[avg] < target)// Procura na direita
        min = avg + 1;
    else if(_TEMPERATURE_ADC_READ[avg] > target)// Procura na esquerda
        max = avg - 1;
    return _binary_search(min, max, target);
}

uint8_t _find_adc_read_index_bin(uint32_t adc_val)
{
    uint8_t min, max;
    min = 0;
    max = _TEMPERATURE_N_SAMPLES;

    return _binary_search(min, max, adc_val);
}

uint8_t _find_adc_read_index(uint32_t adc_val)
{
    uint8_t i, index = 0;

    for(i=0; i < _TEMPERATURE_N_SAMPLES; ++i)
    {
        if(abs(adc_val - _TEMPERATURE_ADC_READ[index]) >
                abs(adc_val - _TEMPERATURE_ADC_READ[i]) )
        {
            index = i;
        }
    }
    return index;
}

int main()
{
    uint32_t val = 2000;

    uint8_t index = _find_adc_read_index_bin(val);

    printf("Valor: %d, Índice: %d, Valor encontrado: %d\n", val, index,
            _TEMPERATURE_ADC_READ[index]);

    return 0;
}
