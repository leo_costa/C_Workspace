#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct agenda {
    char nome[200];
    char tel[30];
}agenda;

#define tamanhoCabecalho 57 // 29+28
#define tamanhoMeio 11

void leArquivo()
{
    FILE *fp;
    fp = fopen("agenda.bin", "rb");
    agenda aux;

    while(1)
    {
        fseek(fp, tamanhoCabecalho*sizeof(char) + sizeof(int), SEEK_CUR);
        fread(aux.nome, sizeof(aux.nome), 1, fp);

        fseek(fp, tamanhoMeio*sizeof(char), SEEK_CUR);
        fread(aux.tel, sizeof(aux.tel), 1, fp);

        if(feof(fp))
            break;

        fseek(fp, 1*sizeof(char), SEEK_CUR); // \n depois do telefone

        printf("Nome: %s | Telefone: %s\n", aux.nome, aux.tel);
    }
    return;
}

int main(void)
{
    int c;
    agenda contatos[5];
    FILE *fp;

    printf("Digite os elementos da agenda:\n");

    for(c = 0; c < 5; c++) 
    {
        printf("\n\nElemento %d: \n\n", c);
        printf("Nome? ");
        fgets(contatos[c].nome, 255, stdin);
        printf("Telefone? ");
        fgets(contatos[c].tel, 255, stdin);
    }
    printf("\n\n Mostrando dados...\n\n");

    for(c = 0; c < 5; c++) 
    {
        printf("\n\n**************** Elemento %d: *****************\n",c);
        printf("Nome: \t%s\n", contatos[c].nome);
        printf("Telefone:\t%s\n", contatos[c].tel);
    }

    fp = fopen("agenda.bin", "wb");

    if( fp != NULL)
    {
        for (c = 0; c < 5; ++c)
        {
            fwrite("\n\n**************** Elemento ", 29*sizeof(char), 1, fp);            
            fwrite(&c, sizeof(int), 1, fp);
            fwrite(": *****************\nNome: \t", 28*sizeof(char), 1, fp);
            fwrite(contatos[c].nome, sizeof(contatos[c].nome), 1, fp);

            fwrite("\nTelefone:\t", 11*sizeof(char), 1, fp);
            fwrite(contatos[c].tel, sizeof(contatos[c].tel), 1, fp);
            fwrite("\n", sizeof(char), 1, fp);
        }
        printf("\n\n Fim \n\n");
        fclose(fp);
        leArquivo();
    }

    return 0;
}