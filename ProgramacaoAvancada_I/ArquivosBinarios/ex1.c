#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *fp;

    fp = fopen("arquivo.txt", "w+");
    fputs("Aula 08 - Arquivos", fp);

    fseek(fp, 9, SEEK_SET);
    fputs(" 01 Funcao fseek", fp);
    fclose(fp);

    fp = fopen("arquivo.txt", "r");
    while (1)
    {
        char c = fgetc(fp);

        if(feof(fp))
            break;

        printf("%c", c);
    }
    printf("\n\n");
    fclose(fp);
    return 0;
}