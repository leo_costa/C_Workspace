#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    FILE *fp;
    char c[] = "Aula 08 - 2 Funcao fread";
    char buffer[100];

    fp = fopen("Arquivo2.txt", "w+");

    fwrite(c, strlen(c) + 1, 1, fp);

    fseek(fp, 0, SEEK_SET);

    fread(buffer, strlen(c)+1, 1, fp);
    printf("%s\n", buffer);
    fclose(fp);
    return 0;
}