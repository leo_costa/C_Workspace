/**
 * @file Atividade1.c
 * @author Leonardo da Silva Costa (unieleocosta@fei.edu.br) - 11.216.091-6 - Turma 630
 * @brief Código para a atividade 1/2 da disciplina CC8210 Programação Avançada 1
 * @version 0.1
 * @date 2020-04-12
 */

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>

#define vetSize 100

void recebeDados(int *vp)
{
    printf("Insira os valores a serem armazenados no vetor:\n");

    int i;
    for (i = 0; i < vetSize; ++i)
    {
        printf("Vetor[%d] = ", i);
        scanf("%d", vp);
        vp++;
    }
}

void mostraDados(int *vp)
{
    printf("Valores inseridos: \n");

    int i;
    for (i = 0; i < vetSize; ++i)
    {
        printf("Vetor[%d] = %d\n", i, *vp);
        vp++;
    }
}

void multiplosSete(int *vp)
{
    vp = vp + (vetSize - 1);

    printf("Numeros multiplos de 7:");

    int i;
    for (i = vetSize; i > 0; --i)
    {
        if(*vp % 7 == 0 && *vp != 0)
        {
            printf(" %d", *vp);
        }
        
        vp--;
    }
}

void pares(int *vp)
{
    printf("\nNumeros pares:");

    int i;
    for (i = 0; i < vetSize; ++i)
    {
        if(*vp % 2 == 0)
        {
            printf(" %d", *vp);
        }
        
        vp++;
    }
}

void somatorio(int *vp)
{
    printf("\nSomatorio:");

    int i, soma = 0;
    for (i = 0; i < vetSize; ++i)
    {
        soma += *vp;
        vp++;
    }

    printf(" %d\n", soma);
}

int main()
{
    int *vet, *inicio;

    vet = (int *) malloc(vetSize * sizeof(int));
    inicio = vet;

    recebeDados(vet);
    mostraDados(vet);
    multiplosSete(vet);
    pares(vet);
    somatorio(vet);

    getchar(); // system("pause") se estiver no windows
    return 0;
}