#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void main()
{
	float x,y;

	printf("Digite as coordenadas x,y do ponto:");
	scanf("%f %f", &x, &y);
	printf("P = (%.2f,%.2f)\n", x, y);

	float tan = 0;
	tan = atan2(y,x)*180/M_PI;
	printf("Angulo = %.2f\n", tan);

	if(tan >= 0 && tan <= 90)
		printf("Quadrante B\n");
	else if(tan > 90 && tan <= 180)
		printf("Quadrante A\n");
	else if(tan < 0 && tan >= -90)
		printf("Quadrante D\n");
	else
		printf("Quadrante C\n");
	getchar();
}
