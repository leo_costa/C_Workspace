#include<stdlib.h>
#include<stdio.h>
#include<math.h>

int main()
{
    float valorCompra      = 0, 
        valorRecebido      = 0;
    int trocoReais[]       = {100, 50, 20, 10, 5, 2, 1};
    int quantidadeNotas[]  = {0, 0, 0 , 0, 0, 0, 0};
    int trocoCentavos[]    = {50, 25, 10, 5, 1};
    int quantidadeMoedas[] = {0, 0 , 0, 0, 0};

    printf("Digite o valor da compra: ");
    scanf("%f", &valorCompra);

    printf("Digite o valor em dinheiro para pagar a compra: ");
    scanf("%f", &valorRecebido);

    if(valorRecebido > valorCompra)
    {
        valorCompra = valorRecebido - valorCompra;
        printf("Calculando o troco para %.2f Reais\n", valorCompra);

        // Enquanto houver troco em notas calcula o troco
        int tipoNota = 0;
        while (valorCompra >= 1) 
        {
            quantidadeNotas[tipoNota] = (int)truncf(valorCompra) / trocoReais[tipoNota];
            if(quantidadeNotas[tipoNota] != 0)
                printf("%d notas de %d R$\n", quantidadeNotas[tipoNota], trocoReais[tipoNota]);
            valorCompra = valorCompra - quantidadeNotas[tipoNota]*trocoReais[tipoNota];
            tipoNota++;
        }
        printf("Falta o troco para %.2f Reais\n", valorCompra);

        valorCompra *= 100;
        int valorMoedas = (int)roundf(valorCompra);

        printf("Calculando o troco para %.2f Centavos\n", valorCompra);

        // Enquanto houver troco em moedas calcula o troco
        int tipoMoeda = 0;
        while (valorMoedas > 0)
        {
            quantidadeMoedas[tipoMoeda] =  valorMoedas / trocoCentavos[tipoMoeda];
            if(quantidadeMoedas[tipoMoeda] != 0)
                printf("%d moedas de %d Centavos\n", quantidadeMoedas[tipoMoeda], trocoCentavos[tipoMoeda]);
            valorMoedas = valorMoedas - quantidadeMoedas[tipoMoeda]*trocoCentavos[tipoMoeda];
            tipoMoeda++;
        }
    }
    else
    {
        printf("O valor recebido não é suficiente para pagar a conta!\n");
    }

    return 0;
}