#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void main()
{
	float a,b,c;
	printf("Digite o valor de a:");
	scanf("%f", &a);
	printf("Digite o valor de b:");
	scanf("%f", &b);
	printf("Digite o valor de c:");
	scanf("%f", &c);

	float val = pow(a,2) + pow(b,2) + pow(c,2);
	printf("a2 + b2 + c2 = %.2f\n", val);
	getchar();
}
