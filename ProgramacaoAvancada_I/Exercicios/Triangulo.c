#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void main()
{
	float lados[]={0,0,0};

	int i=0, j=0;
	for(i; i<3; ++i)
	{
		printf("Digite o valor de um dos lados:");
		scanf("%f", &lados[i]);
	}
		
	for(i = 0; i < 2; ++i)
	{
		for(j=i+1; j < 3; ++j)
		{
			float aux = 0;
			if(lados[i] < lados[j])
			{
				aux = lados[i];
				lados[i] = lados[j];
				lados[j] = aux;
			}
		}
	}

	printf("Lados Ordenados = %.2f, %.2f, %.2f\n", lados[0], lados[1], lados[2]);

	if(lados[0] >= lados[1] + lados[2])
		printf("NAO FORMA TRIANGULO\n");
	else
	{
		if(round(pow(lados[0],2)) == round(pow(lados[1],2)) + round(pow(lados[2],2)))
			printf("TRIANGULO RETANGULO\n");
		else if(pow(lados[0],2) > pow(lados[1],2) + pow(lados[2],2))
			printf("TRIANGULO OBTUSANGULO\n");
		else if(pow(lados[0],2) < pow(lados[1],2) + pow(lados[2],2))
			printf("TRIANGULO ACUTANGULO\n");
		if(round(lados[0]) == round(lados[1]) && round(lados[1]) == round(lados[2]) )
			printf("TRIANGULO EQUILATERO\n");
		else if(round(lados[0]) == round(lados[1]) || round(lados[1]) == round(lados[2]) || round(lados[0]) == round(lados[2]))
			printf("TRIANGULO ISOSCELES\n");
	}
	getchar();
}
