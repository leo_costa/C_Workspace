/**
 * @file advinhaNum.c
 * @author Leonardo da Silva Costa (unieleocosta@fei.edu.br) - 11.216.091-6
 * @brief Exercício da aula de laboratório de Programação Avançada.
 * Recebe um número a ser adivinhado, limpa a tela, recebe números do usuário até que
 * ele digite o número correto, informando se o número digitado é maior ou menor que o valor correto.
 * Quando o valor correto for inserido, mostra a quantidade de tentativas até acertar.
 * @version 0.0
 * @date 2020-03-08
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include<stdlib.h>
#include<stdio.h>

int main()
{
    int numAdv = 0;      /** Número a ser adivinhado. */
    int numDig = 0;      /** Número digitado pelo usuário. */
    int nTentativas = 0; /** Quantidade de tentativas. */

    /**
     * Recebe o número a ser adivinhado.
     */
    printf("Digite o numero a ser adivinhado: ");
    scanf("%d", &numAdv);

    /**
     * Limpa a tela do console.
     */
    system("clear");

    /**
     * Recebe o número digitado e enquanto ele não for igual o adivinhado,
     * informa se ele é maior ou menor que o valor correto.
     */
    do
    {
        printf("Digite o seu palpite: ");
        scanf("%d", &numDig);

        if(numDig > numAdv)
            printf("O numero digitado é maior que o numero correto!\n");
        else if(numDig < numAdv)
            printf("O numero digitado é menor que o numero correto!\n");

        nTentativas++;
    } while (numDig != numAdv);
    

    /**
     * Mostra a quantidade de tentativas até acertar o número.
     */
    printf("Voce acertou o numero! A quantidade de tentativas foi = %d \n", nTentativas);

    getchar();
    return 0;
}