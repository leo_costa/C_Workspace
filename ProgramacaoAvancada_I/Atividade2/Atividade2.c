/**
 * @file Atividade2.c
 * @brief Código para a atividade 2/2 da disciplina CC8210 Programação Avançada 1
 * @version 0.2
 * @date 2020-05-04
 */

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <sys/stat.h>
#include <string.h>

#define FIM_VET -123456789

int contaSeparador(char *str, char separador)
{
    int i, count=0;
    for (i = 0; i < strlen(str); ++i)
    {
        if(*(str+i) == separador)
            count++;
    }
    return count;
}

void gravaBin(char* dados, int tamanhoArq)
{
    FILE *fp;
    // Grava em um arquivo binário
    fp = fopen("stringFB.bin", "wb");
    if (fp != NULL)
    {
        fwrite(dados, tamanhoArq*sizeof(char), 1, fp);
        fclose(fp);
    }
}

int* stringParaVetorInt(char *dados)
{
    // Transforma string em vetor de int
    const char sep[1] = ",";
    char* val;
    int num, *inicio, *vetNumeros;

    int nNumeros = contaSeparador(dados, ',') + 2;

    vetNumeros = (int *)malloc(nNumeros*sizeof(int));
    inicio = vetNumeros;

    val = strtok(dados, sep);

    while (val != NULL)
    {
        num = atoi(val);
        *vetNumeros = num;

        vetNumeros++;
        val = strtok(NULL, sep);
    }

    *vetNumeros = FIM_VET;// Indica o fim do vetor de int
    vetNumeros = inicio;

    return vetNumeros;
}

int* leDados(void)
{
    printf("Lendo os valores do arquivo...");

    FILE *fp;
    fp = fopen("stringF.txt","r");
    char *dados = NULL;
    int *vetNumeros = NULL, *inicio = NULL;

    if(fp != NULL)
    {
        struct stat st;
        stat("stringF.txt", &st);
        int tamanhoArq = (int)st.st_size + 1;
        dados = (char *)malloc(tamanhoArq*sizeof(char));

        fgets(dados, tamanhoArq, fp);
        printf("\nDados lidos!");
        fclose(fp);

        printf("\nGravando dados no arquivo binario...");
        gravaBin(dados, tamanhoArq);

        printf("\nConvertendo string para vetor de int...");
        vetNumeros = stringParaVetorInt(dados);

        free(dados);
    }
    else
    {
        printf("\n\tFalha ao ler o arquivo");
        exit(1);
    }
    return vetNumeros;
}

void mostraDados(int *vp)
{
    printf("\nValores inseridos: \n");
    int i=0;
    while( *(vp+i) != FIM_VET)
    {
        printf("Vetor[%d] = %d ", i, *(vp+i));
        i++;

        if(i%10 == 0) printf("\n");
    }
}

void multiplosSete(int *vp)
{
    int i=0;
    while( *(vp+i) != FIM_VET) i++;
    i--;

    printf("Numeros multiplos de 7:");

    for (i; i >= 0; --i)
    {
        if(*(vp+i) % 7 == 0 && *(vp+i) != 0)
        {
            printf(" %d", *(vp+i));
        }
    }
}

void pares(int *vp)
{
    printf("\nNumeros pares:");

    int i=0;
    while( *(vp+i) != FIM_VET)
    {
        if(*(vp+i) % 2 == 0)
        {
            printf(" %d", *(vp+i));
        }
        i++;
    }
}

void somatorio(int *vp)
{
    printf("\nSomatorio:");

    int i=0, soma = 0;
    while( *(vp+i) != FIM_VET)
    {
        soma += *(vp+i);
        i++;
    }

    printf(" %d\n", soma);
}

void salvaSomatorio(int soma)
{
    printf("\nSalvando resultado no arquivo binario...");
    FILE *fp;
    fp = fopen("resultSoma.bin", "wb");

    if(fp != NULL)
    {
        fwrite(&soma, sizeof(int), 1, fp);
        fclose(fp);
    }
}

void leSomatorio(void)
{
    printf("\nMostrando resultado a partir do arquivo binario...");
    FILE *fp;
    int soma;
    fp = fopen("resultSoma.bin", "rb");

    if(fp != NULL)
    {
        fread(&soma, sizeof(int), 1, fp);
        fclose(fp);
    }
    printf("\nResultado da somatoria = %d\n", soma);
}

int main()
{
    int *vet;

    vet = leDados();
    if(vet != NULL)
    {
        int somat = 0, i = 0;
        while( *(vet+i) != FIM_VET)
        {
            somat += *(vet+i);
            ++i;
        }
        salvaSomatorio(somat);
    }

    mostraDados(vet);
    multiplosSete(vet);
    pares(vet);
    // somatorio(vet);
    leSomatorio();

    getchar(); // system("pause") se estiver no windows
    return 0;
}