#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

char* resizeVetor(char *vetor, int *tamanho)
{
    vetor = (char *) realloc(vetor, (2* *tamanho + 1) * sizeof(char));
    *tamanho = 2* *tamanho;
    return vetor;
}

int main()
{
    system ("/bin/stty raw");
    char *vetor, dado;
    int tamanho = 10, recebidos = 0;

    vetor = (char *) malloc((tamanho+1) * sizeof(char));

    printf("Digite uma sequencia de caracteres: ");

    while(1)
    {
        dado = getchar();

        if(dado == '.')
            break;
        else
        {
            if(recebidos == tamanho)
            {
                vetor = resizeVetor(vetor, &tamanho);
            }
                
            *(vetor+recebidos) = dado;
            recebidos++;
        }

    }

    system ("/bin/stty cooked");
    *(vetor+recebidos) = 0;

    printf("\nCaracteres digitados: %s \n", vetor);

    return 0;
}