#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

void resizeVetor(char *vetor, int *tamanho)
{
  char *vetAux, *inicio;
  vetAux = (char *) malloc(2* *tamanho * sizeof(char));
  inicio = vetAux;

  int i;

  for(i = 0; i < tamanho; ++i)
  {
    *vetAux = *vetor;
    vetAux++;
    vetor++;
  }

  vetor = inicio;
}

int main()
{
  system ("/bin/stty raw");
  char *vetor, *inicio;

  vetor = (char *) malloc(11 * sizeof(char));
  inicio = vetor;

  int i;
  printf("Digite uma sequencia de caracteres: ");
  for(i = 0; i < 10; ++i)
  {
    *vetor = getchar();
    vetor++;
  }
  system ("/bin/stty cooked");
  *vetor = 0;
  vetor = inicio;

  printf("\nCaracteres digitados: %s \n", vetor);

  return 0;
}