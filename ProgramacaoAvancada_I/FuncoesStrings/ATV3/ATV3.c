#include<stdlib.h>
#include<stdio.h>

void print_result(int res)
{
    printf("O resultado e igual a %d\n", res);
}

int add(int num1, int num2)
{   
    int res = num1 + num2;
    print_result(res);
    return res;
}

int subtract(int num1, int num2)
{
    int res = num1 - num2;
    print_result(res);
    return res;
}

int multiply(int num1, int num2)
{
    int res = num1 * num2;
    print_result(res);
    return res;
}

int divide(int num1, int num2)
{
    int res = num1 / num2;
    print_result(res);
    return res;
}

int absoluto(int num)
{
    int res = num;
    if(num < 0)
        res *= -1;
    print_result(res);
    return res;
}

int fat(int num)
{
    if(num > 1)
        return num*fat(num-1);
    return 1;
}

int fatorial(int num)
{
    int res = fat(num);
    print_result(res);
    return res;
}

int main()
{
    char opcao = 0; // Opcao de calculo
    int num1, num2; // Valores fornecidos
    int val;        // Valor calculado

    while(opcao != 0x1B) // 0x1B = ESC
    {
        printf("Insira o primeiro numero: ");
        scanf("%d%*c", &num1);// Consome o numero digitado + \n que fica no buffer

        printf("Insira o segundo numero: ");
        scanf("%d%*c", &num2);
        
        printf("Escolha uma das seguintes opcoes:\n");
        printf("\ta - Adicao\n\ts - Subtracao\n\tm - Multiplicacao\n\td - Divisao\n\tA - Absoluto\n\tf - Fatorial\n\tOu pressione ESC para sair\n Opcao: ");

        scanf("%c%*c", &opcao);

        switch (opcao)
        {
        case 'a':
            val = add(num1, num2);
            break;
        case 's':
            val = subtract(num1, num2);
            break;
        case 'm':
            val = multiply(num1, num2);
            break;
        case 'd':
            val = divide(num1, num2);
            break;
        case 'A':
            val = absoluto(num1);
            val = absoluto(num2);
            break;
        case 'f':
            val = fatorial(num1);
            val = fatorial(num2);
            break;
        default:
            break;
        }
    }
}