#include<stdlib.h>
#include<stdio.h>

int soma(int x)
{
    int val = 0;
    int i;

    for (i = 1; i <= x; ++i)
    {
        val += i;
    }
    return val;
}

void quad(int x)
{
    char val = 'x';//0xDB;
    int i,j;
    for (i = 0; i < x; ++i)
    {
        for (j = 0; j < x; ++j)
        {
            printf("%c", val);    
        }
        printf("\n");
    }
}

int par(int x)
{
    if(x % 2 == 0)
        return 1;
    return 0;
}

int pot(int x, int y)
{
    int val = 1;
    if(y > 1)
        return x*pot(x, y-1);
    return x;
}

void main(void)
{
    int x,s,p;
    x = 100;

    s = soma(x);
    printf("Soma = %d\n", s);
    quad(x);

    if(par(s) == 1)
        printf("A soma e par!\n");
    else
        printf("A soma e impar\n");

    p = pot(x, 3);
    printf("%d^%d = %d\n", x, 3, p);
}