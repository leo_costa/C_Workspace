#include<stdlib.h>
#include<stdio.h>

int iContaVogais(char *frase)
{
    char vogais[] = {'a','e','i','o','u'};
    int nVogais = 0;

    while(*frase != 0)
    {
        int j;
        for (j = 0; j < 6; ++j)
        {
            if(*frase == vogais[j])
                nVogais++;
        }
        frase++;
    }
    return nVogais;
}

int iContaFEI(char *frase)
{
    char FEI[] = {'f','e','i'};
    int nFEI = 0;

    while (*frase != 0)
    {
        int palavraCompleta = 0;

        if(*frase == FEI[palavraCompleta])
        {   
            palavraCompleta++;
            int i;
            for (i = 1 ; i < 3; ++i)
            {
                frase++;
                if(*frase == FEI[i])
                    palavraCompleta++;
            }
            
            if(palavraCompleta == 3)
                nFEI++;
        }
        else
            frase++;
    }
    return nFEI;
}

int iTamanhoFrase(char *frase)
{
    int tamFrase = 0;
    while (*frase != 0)
    {
        tamFrase++;
        frase++;
    }
    return tamFrase-1;
}

int main()
{
    char frase[200];

    printf("Insira uma frase somente em letras minusculas: ");
    fgets(frase,200,stdin);
    // scanf("%s", frase); - Näo funciona, a função scanf 
                        // interrompe a string no primeiro espaço em branco

    int qtdVogais = iContaVogais(frase);
    int qtdFEI = iContaFEI(frase);
    int tamFrase = iTamanhoFrase(frase);

    printf("A frase inserida possui %d vogais!\n", qtdVogais);
    printf("A frase inserida possui %d vezes a palavra fei!\n", qtdFEI);
    printf("O tamanho da frase inserida eh %d caracteres (contando os espacos)!\n", tamFrase);

    getchar();
    return 0;
}