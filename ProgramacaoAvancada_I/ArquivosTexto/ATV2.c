#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main()
{
    FILE *fp;
    char *filename;
    filename = (char *) malloc(200 * sizeof(char));

    printf("Insira o nome do arquivo a ser lido: ");
    // scanf("%s", filename);
    fgets(filename, 255, stdin);
    strtok(filename, "\n");

    fp = fopen(filename, "r");

    if(fp != NULL)
    {
        char *conteudoArquivo, *linha, *inicio;

        long pos = ftell(fp);    // Current position
        fseek(fp, 0, SEEK_END);    // Go to end
        long tamanhoArquivo = ftell(fp) - pos;
        fseek(fp, pos, SEEK_SET);  // restore original position


        conteudoArquivo = (char *) malloc(tamanhoArquivo * sizeof(char));
        linha = (char *) malloc(255 * sizeof(char));

        while (fgets(linha, 255, fp) != NULL)
        {
            strcat(conteudoArquivo, linha);
        }
        fclose(fp);

        printf("Conteudo sem alteracao:\n%s\n----------\n", conteudoArquivo);

        inicio = conteudoArquivo;

        while (*conteudoArquivo != 0)
        {
            if(*conteudoArquivo == 'a')
                *conteudoArquivo = 'e';
            conteudoArquivo++;
        }

        conteudoArquivo = inicio;

        fp = fopen("arquivoAlterado.txt", "w");
        fputs(conteudoArquivo, fp);
        fclose(fp);
    }
    else
    {
        printf("Erro ao abrir o arquivo!\n");
    }
    
}