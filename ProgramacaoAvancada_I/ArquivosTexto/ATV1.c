#include <stdlib.h>
#include <stdio.h>

int main()
{
    FILE *fp;
    char *nomeArq = (char *) malloc(50 * sizeof(char));

    printf("Insira o nome do arquivo: ");
    scanf("%s", nomeArq);

    fp = fopen(nomeArq, "r");

    if(fp != NULL)
    {
        char *linha, *erro;
        printf("Conteudo do arquivo: %s\n", nomeArq);

        while(1)
        {
            erro = fgets(linha, 255, fp);

            if (erro != NULL)
                printf("%s", linha);
            else
                break;
        }
        fclose(fp);
    }
    else
    {
        printf("Erro ao abrir o arquivo!\n");
    }

    return 0;
}