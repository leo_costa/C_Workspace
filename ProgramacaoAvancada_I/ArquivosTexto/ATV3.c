#include <stdlib.h>
#include <stdio.h>
#include <string.h>

long lTamanhoArquivo(FILE *f)
{
    long pos = ftell(f);        // Posição atual de leitura do arquivo
    fseek(f, 0, SEEK_END);      // Vai para a posição final de leitura (fim do arquivo)
    long size = ftell(f) - pos; // Tamanho = Posição atual - anterior
    fseek(f, pos, SEEK_SET);    // Retorna o arquivo para a posição inicial
    return size;
}

int main()
{
    FILE *fp; // Ponteiro para o arquivo a ser lido/escrito
    char *nomeLeitura, *nomeEscrita, *conteudoLido;

    int nPalavras   = 0;
    int nCaracteres = 0;
    int nEspacos   = 0;

    nomeLeitura = (char *) malloc(200 * sizeof(char));
    nomeEscrita = (char *) malloc(200 * sizeof(char));

    printf("Digite o nome do arquivo para leitura: ");
    fgets(nomeLeitura, 200, stdin);
    strtok(nomeLeitura, "\n"); // Remove o \n do final

    printf("Digite o nome do arquivo para escrita: ");
    fgets(nomeEscrita, 200, stdin);
    strtok(nomeEscrita, "\n"); // Remove o \n do final

    fp = fopen(nomeLeitura, "r");

    if(fp != NULL)
    {
        long tamanhoArquivo = lTamanhoArquivo(fp);
        char *linha;
        char ultimoCharLido = 0;

        conteudoLido = (char *) malloc( tamanhoArquivo * sizeof(char));
        linha = (char *) malloc(255 * sizeof(char));

        while (fgets(linha, 255, fp) != NULL)
            strcat(conteudoLido, linha); // Concatena as linhas lidas em um único ponteiro
        fclose(fp);


        while (*conteudoLido != 0)
        {
            if(*conteudoLido == ' ' || *conteudoLido == '\n')
            {
                if(*conteudoLido == ' ')
                    nEspacos++;

                if(ultimoCharLido != ' ' && ultimoCharLido != 0)
                    nPalavras++;
            }
            else
            {
                nCaracteres++;
            }
            ultimoCharLido = *conteudoLido;
            conteudoLido++;
        }

        if(ultimoCharLido != ' ' && ultimoCharLido != 0)
            nPalavras++;

        fp = fopen(nomeEscrita, "w");

        if(fp != NULL)
        {
            fprintf(fp, "********** ARQUIVO: %s **********\n", nomeLeitura);
            fprintf(fp, "Quantidade de palavras: %d\n", nPalavras);
            fprintf(fp, "Quantidade de caracteres: %d\n", nCaracteres);
            fprintf(fp, "Quantidade de espacos: %d\n", nEspacos);
            fprintf(fp, "*********************************");
            fclose(fp);
        }
        else
        {
            printf("Erro ao criar arquivo!\n");
            exit(1);
        }
    }
    else
    {
        printf("Erro ao abrir o arquivo\n");
        exit(1);
    }
    return 0;
}