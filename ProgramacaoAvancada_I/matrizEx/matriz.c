#include <stdlib.h>
#include <stdio.h>

enum
{
    SOMA_TOTAL              = 0,
    SOMA_DIAGONAL_PRINCIPAL = 1,
    SOMA_PARES              = 2
};

int main()
{
    int mSize            = 0;
    int valorCalculos[3] = {0, 0, 0};
    int tipoCalculo      = 3;
    int r                = 0;
    int c                = 0;

    printf("Digite a ordem da matriz: ");
    scanf("%d", &mSize);
    int iMat[mSize][mSize];

    for (r = 0; r < mSize; ++r)
    {
        for (c = 0; c < mSize; ++c)
        {
            printf("Digite o elemento %d da matriz iMat[%d][%d] = ", mSize*r + c + 1, r, c);
            scanf("%d", &iMat[r][c]);
            valorCalculos[SOMA_TOTAL] += iMat[r][c];

            if(r == c)
                valorCalculos[SOMA_DIAGONAL_PRINCIPAL] += iMat[r][c];

            if(iMat[r][c] % 2 == 0)
                valorCalculos[SOMA_PARES] += iMat[r][c];
        }        
    }

    printf("\nVoce digitou a seguinte matriz:\n");

    for (r = 0; r < mSize; ++r)
    {
        for( c = 0; c < mSize; ++c)
        {
            if( c == 0)
                printf("| ");

            printf("%d ", iMat[r][c]);

            if(c == mSize-1)
                printf("|");
        }
        printf("\n");
    }
    

    printf("\nEscolha o cálculo a ser realizado:\n\t");
    printf("1 - Soma de todos os elementos da matriz\n\t");
    printf("2 - Soma dos termos da diagonal principal\n\t");
    printf("3 - Soma dos termos pares \n");

    scanf("%d", &tipoCalculo);

    while (tipoCalculo > 3)
    {
        printf("Digite uma opção valida: ");
        scanf("%d", &tipoCalculo);
    }
    
    printf("O valor do calculo e\x27: %d \n", valorCalculos[tipoCalculo-1]);
    getchar();
    return 0;
}