#include "pilha.h"

Pilha::Pilha()
{
    this->pilha = nullptr;
    this->posicaoAtual = nullptr;
    this->tamanho = 0;
}

Pilha::~Pilha()
{
    if (tamanho > 0)
    {
        free(this->pilha);
        std::cout << "Pilha desalocada" << std::endl;
    }
}

void Pilha::push(int val)
{
    this->tamanho++;
    if (tamanho == 1)
    {
        this->pilha = (int *)malloc(tamanho * sizeof(int));
    }
    else
    {
        this->pilha = (int *)realloc(this->pilha, tamanho * sizeof(int));
    }
    this->posicaoAtual = this->pilha + tamanho - 1;
    *this->posicaoAtual = val;
    std::cout << "Pushed: " << val;
}

int Pilha::pop()
{
    int val = -1;

    if (tamanho > 0)
    {
        val = *this->posicaoAtual;
        this->tamanho--;
        this->pilha = (int *)realloc(this->pilha, tamanho * sizeof(int));
        this->posicaoAtual = this->pilha + tamanho - 1;
    }
    else
    {
        std::cout << "\nPilha Vazia!!!\n";
    }
    return val;
}

int Pilha::getTamanho()
{
    return this->tamanho;
}

int main()
{
    Pilha p;

    std::cout << "\nPreenchendo a pilha:\n";
    for (int i = 0; i < 100; ++i)
    {
        p.push(rand() % 1000);
        std::cout << "\t";

        if ((i + 1) % 10 == 0)
            std::cout << std::endl;
    }

    std::cout << "\nO tamanho da pilha é " << p.getTamanho() << std::endl;
    std::cout << "\nRetirando valores da pilha:\n";

    for (int i = 0; i < 100; ++i)
    {
        std::cout << "Popped: " << p.pop() << "\t";

        if ((i + 1) % 10 == 0)
            std::cout << std::endl;
    }
    std::cout << std::endl;
}