#include <stdlib.h>
#include <iostream>
#include <random>

class Pilha
{
private:
    int *pilha;
    int *posicaoAtual;
    int tamanho;

public:
    Pilha();
    ~Pilha();

    void push(int val);
    int pop();
    int getTamanho();
};
