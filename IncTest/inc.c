#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int vec[5] = {0, 0, 0, 0, 0};
    char str[] = "Teste";
    int a = 0;

    vec[a] = 1;
    vec[a++] = 2;
    vec[++a] = 3;

    int i;
    for (i = 0; i < 5; ++i)
        printf("%d ", vec[i]);
    printf("\n");

    printf("strlen(\"Teste\") = %d\n", (int)strlen(str));
}
