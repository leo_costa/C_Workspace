#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *bin_file_p;

    bin_file_p = fopen("bin_data.bin", "wb");

    if(bin_file_p != NULL)
    {
        unsigned char data = 0;

        for(data = 0; data < 255; ++data)
        {
            fwrite(&data, sizeof(char), 1, bin_file_p);
        }
        data = 0;
        fwrite(&data, sizeof(char), 1, bin_file_p);
        fclose(bin_file_p);
    }
}
