#include <string.h>
#include <stdio.h>

#define MAX_SZ 10

int main()
{
    char str_a[] = "Compare the end of the string - Falhou";
    short int str_sz = strlen(str_a);

    char aux[MAX_SZ] = "", comp_str[MAX_SZ] = " - Falhou";

    if(str_sz > strlen(comp_str))
    {
        memcpy(aux, &str_a[str_sz - strlen(comp_str)], strlen(comp_str));
        aux[strlen(comp_str)] = 0;
    }

    if(!strcmp(aux, comp_str))
    {
        printf("aux == comp_str\n");
    }
    else
    {
        printf("aux != comp_str\n");
    }

    printf("%s\n", aux);
    return 0;
}
