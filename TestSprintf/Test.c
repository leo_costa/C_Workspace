#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char aux[50];
    char string_val[10];
    int testeArray[100];

    int val = 999999999;
    float val2 = 110.6789;

    sprintf(string_val, "%.3f", val2);
    sprintf(aux, "Teste sprintf: %s", string_val);
    printf("%s\n", aux);

    printf("Array length: %ld", sizeof(testeArray)/sizeof(testeArray[0]));

    return 1;
}
