#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define JSON_MAX_SIZE 6500

#define RED "\033[0;31m"
#define BLUE "\033[0;34m"
#define GREEN "\033[0;92m"
#define RESET "\033[0m"

void readJsonFile(char jsonParams[])
{
    char filename[] = "/home/leonardo/SGRIDD/jsonParams.json";
    FILE *fp;
    fp = fopen(filename, "r");

    if (fp != NULL)
    {
        char *linha;
        linha = (char*)malloc(255*sizeof(char));
        int jsonI = 0;

        while(fgets(linha, 255, fp) != NULL && jsonI < JSON_MAX_SIZE)
        {
            int i;
            for (i = 0; i < strlen(linha) && jsonI < JSON_MAX_SIZE; ++i, ++jsonI)
            {
                jsonParams[jsonI] = *(linha+i);
            }
        }
        jsonParams[jsonI] = 0;
        free(linha);

    }
}

void remove_spaces(char* s)
{
    const char* d = s;
    do
    {
        while (*d == ' ' || *d == '\n')
            ++d;

    } while((*s++ = *d++));
}

int count_delimiters(char data[])
{
    int curly = 0;
    int square = 0;

    int i;
    for (i = 0; i < strlen(data); ++i)
    {
        if(data[i] == '{')
            curly++;
        else if(data[i] == '}')
            curly--;
        else if(data[i] == '[')
            square++;
        else if(data[i] == ']')
            square--;
    }

    return square+curly;
}

void remove_brackets(char s[])
{
    const char *d = s;
    int len = strlen(s);
    int i;

    for(i = 1; i <= len; ++i)
    {
        s[i-1] = d[i];
    }
    s[len-2] = 0;
}

int find_key(char data[], int start_index, char key[])
{
    int i = start_index;
    int key_start = -1;

    while((data[i] != '"' || key_start == -1) && data[i] != 0)
    {
        if(data[i] == '"' && key_start == -1)
            key_start = i+1;
        i++;
    }

    int j, k = 0;
    if(key_start != -1 && data[i+1] == ':')
    {
        for (j = key_start; j < i; ++j, ++k)
        {
            key[k] = data[j];
        }
        key[k] = 0;
    }
    else
        key[0] = 0;

    if(data[i] == 0)
        i = -1;
    else
        i++;

    return i;
}

int get_key_contents(char data[], int key_end, char key_content[])
{
    int open_curly = 0;
    key_end++;

    int content_end = key_end;
    char delim_start = ' ', delim_end = ' ';

    do {
        if(delim_start == ' ')
        {
            if(data[content_end] == '{')
            {
                delim_start = '{';
                delim_end = '}';
            }
            else if (data[content_end] == '[')
            {
                delim_start = '[';
                delim_end = ']';
            }
        }

        if(data[content_end] == delim_start)
            open_curly++;
        else if(data[content_end] == delim_end)
            open_curly--;

        content_end++;
    }while (open_curly != 0);

    int i, j = 0;
    for (i = key_end; i < content_end; ++i, ++j)
    {
        key_content[j] = data[i];
    }

    key_content[j] = 0;

    if(data[content_end+1] == 0)
        content_end = -1;

    return content_end;
}

int get_key_single_value(char data[], int key_end, char key_value[])
{
    key_end++;
    int value_end = key_end;

    if(data[value_end] != '[')
        while(data[value_end] != ',' && data[value_end] != '}')
            value_end++;
    else
    {
        while(data[value_end] != ']')
            value_end++;
        value_end++;
    }

    int i, j = 0;
    for (i = key_end; i < value_end; ++i, ++j)
    {
        key_value[j] = data[i];
    }

    key_value[j] = 0;

    if(data[value_end+1] == 0)
        value_end = -1;
    return value_end;
}

void get_content_values(char data[], char prefix[10], char parent_key[])
{
    int key_value_end = 0;
    char value_key_content[5000], value_key[100];
    char back_prefix[10], back_parent[100];
    sprintf(back_prefix, "%s", prefix);
    sprintf(back_parent, "%s", parent_key);

    while (key_value_end != -1)
    {
        key_value_end = find_key(data, key_value_end, value_key);
        if(strlen(value_key) > 0 )
        {

            if(strcmp(value_key, "cycle") && strcmp(value_key, "sched") &&
                    strcmp(value_key, "timer"))
            {

                key_value_end = get_key_single_value(data, key_value_end,
                                                     value_key_content);
                printf(" %s\t| %s = %s @ %s\n", prefix, value_key, value_key_content, parent_key);
                printf(" %s\t| %s = %s\n", prefix, value_key, value_key_content);
            }
            else
            {
                key_value_end = get_key_contents(data, key_value_end,
                                                 value_key_content);
                if(value_key_content[0] == '[')
                    remove_brackets(value_key_content);

                printf(" \t| %s = %s\n", value_key, value_key_content);

                strcat(prefix, "\t");
                strcat(parent_key, "/");
                strcat(parent_key, value_key);
                get_content_values(value_key_content, prefix, parent_key);
                sprintf(prefix, "%s", back_prefix);
                sprintf(parent_key, "%s", back_parent);
            }
        }
    }
}

int main()
{
    char jsonParams[JSON_MAX_SIZE], key_content[JSON_MAX_SIZE];

    char validKeys[][100] = {"auto_control", "power_control"};
    char key_found[100], prefix[10] = "";

    readJsonFile(jsonParams);
    remove_spaces(jsonParams);
//     printf("%s\n", jsonParams);
//     return 0;
    printf("%s%s\n\n%s Delimiter ok = %d\n Json Size = %lu%s\n\n", GREEN, jsonParams, BLUE,
            count_delimiters(jsonParams), strlen(jsonParams), RESET);

    int key_end = 0;
    while(key_end != -1)
    {
        key_end = find_key(jsonParams, key_end, key_found);
        if(strlen(key_found) > 0)
        {
            int n;
            for (n = 0; n < 2; ++n)
            {
                if(!strcmp(validKeys[n], key_found))
                {
                    key_end = get_key_contents(jsonParams, key_end, key_content);

                    printf("%s: %s\n", key_found, key_content);

                    get_content_values(key_content, prefix, key_found);
                    sprintf(prefix, " ");
                }
            }
        }
    }

    return 0;
}
